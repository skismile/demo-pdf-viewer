import 'package:http/http.dart' as http;
import 'package:path_provider/path_provider.dart';

class ApiService {
  static const String PDF_URL = "http://www.pdf995.com/samples/pdf.pdf";

  static Future<String> loadPDF() async {
    var response = await http.get(Uri.parse(PDF_URL));
    print(response.bodyBytes);
    var dir = await getApplicationDocumentsDirectory();
    print(dir);
    // File file = File("${dir.path}/data.pdf");
    // file.writeAsBytes(response.bodyBytes, flush: true);
    return "file.path";
  }
}
